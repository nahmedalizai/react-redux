import React, {useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {buyCake} from '../Redux'


function HooksCakeContainer() {
    const [number,setNumber] = useState(1)
    const numOfCakes = useSelector(state => state.cake.numOfCakes)
    const dispatch = useDispatch()
    return (
        <div>
            <h2>(With hooks) Number of Cakes - {numOfCakes}</h2>
            <input type='text' value={1} onChange={e => setNumber(e.target.value)}></input>
            <button onClick={() => dispatch(buyCake(number))}>Buy {number} Cake</button>
        </div>
    )
}

export default HooksCakeContainer