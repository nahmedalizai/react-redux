import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchUsers } from '../Redux'

function UserContainer(props) {
    useEffect(() => {
        props.fetchUsers()
        //debugger
        console.log(props.userData)
    },[])

    return (props.userData.loading ?
        (<div>
            <h2>Loading</h2>
        </div>) : props.userData.error ?
            (<div>
                <h2>{props.userData.error}</h2>
            </div>) : (
                <div>
                    <h2>User data</h2>
                    <div>
                        {props.userData && props.userData.users && props.userData.users.map(user => <p key={user.id}>{user.name}</p>)}
                    </div>
                </div>
            )
    )
}

const mapStateToProps = state => {
    return {
        userData: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers: () => dispatch(fetchUsers())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(UserContainer)