import React from 'react'
import { buyIcecream } from '../Redux'
import { connect } from 'react-redux'

function IceCreamContainer(props) {

    return (
        <div>
            <h2>(Without Hooks) Number of Icecreams - {props.numOfIcecreams}</h2>
            <button onClick={props.buyIcecream}>Buy Icecream</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        numOfIcecreams: state.icecream.numOfIcecreams
    }
}

const mapDispatchToProps = dispatch => {
    return {
        buyIcecream: () => dispatch(buyIcecream())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(IceCreamContainer)
