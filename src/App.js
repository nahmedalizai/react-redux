import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './Redux/store';
// import CakeContainer from './Components/CakeContainer';
// import HooksCakeContainer from './Components/HooksCakeContainer';
// import IcecreamContainer from './Components/IceCreamContainer';
// import HooksIcecreamContainer from './Components/HooksIceCreamContainer';
// import ItemContainer from './Components/ItemContainer';
import UserContainer from './Components/UserContainer';
function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <UserContainer/>
        {/* <ItemContainer cake/>
        <ItemContainer/>
        <HooksCakeContainer/>
        <CakeContainer />
        <IcecreamContainer/>
        <HooksIcecreamContainer/> */}
      </div>
    </Provider>
  );
}

export default App;
