import { FETCH_USERS_REQUEST, 
        FETCH_USERS_SUCCESS, 
        FETCH_USERS_ERROR
} from './userTypes'

import axios from 'axios'

// eslint-disable-next-line
const fetchUsersRequest = () => {
    return {
        type : FETCH_USERS_REQUEST
    }
}

// eslint-disable-next-line
const fetchUsersSuccess = users => {
    return {
        type : FETCH_USERS_SUCCESS,
        payload : users
    } 
}

// eslint-disable-next-line
const fetchUsersError = error => {
    return {
        type : FETCH_USERS_ERROR,
        payload : error
    } 
}

// eslint-disable-next-line
export const fetchUsers = () => {
    return function(dispatch) {
        dispatch(fetchUsersRequest)
        axios.get('https://jsonplaceholder.typicode.com/users')
        .then(response => {
            const users = response.data
            dispatch(fetchUsersSuccess(users))
        })
        .catch(error => {
            dispatch(fetchUsersError(error.message))
        })
    }
}